package com.itdfq.dingding;

import com.itdfq.dingding.domain.param.TextMsg;
import com.itdfq.dingding.domain.response.DingResult;
import com.itdfq.dingding.service.DingDingService;

import java.util.Arrays;

/**
 * @author: QianMo
 * @date: 2022/2/11 10:28
 * @mark:
 */
public class Test {
    //https://oapi.dingtalk.com/robot/send?access_token=
    private static final String SECRET = "SEC2f352ffa14fa8ab69ab95525ef841e042699a0be0198344bef2132f9dcee3a988";

    private static final String ACCESS_TOKEN = "5dbf149ddfa5a0b9264fa79d0b71113d44dc0890b7ba98a9aa29c230be983c23f";

    public static void main(String[] args) throws Exception {


        TextMsg textMsg = new TextMsg();
        textMsg.setSecret(SECRET);
        textMsg.setAccessToken(ACCESS_TOKEN);
        textMsg.setAt(new TextMsg.At(Arrays.asList("15670366127"), null, false));
        textMsg.setText(new TextMsg.Text("测试文本消息"));

        DingResult send = DingDingService.send(textMsg);
        System.out.println(send);

    }

}
